#include "../../include/seashell/Seashell.h"
#include <iostream>

using namespace seashell;
using namespace std;

class TailCave : public scene::Dungeon
{
    public:
        TailCave(Game* game);
        void events();
};

class La64 : public Game
{
    public:
        La64();
        ~La64();
};

int main()
{
    La64* game = new La64();
    game->setTitle(L"Link's Awakening 64");
    game->run();
    delete game;

    return 0;
}

TailCave::TailCave(Game* game) : scene::Dungeon(game, "tail_cave")
{

}

void TailCave::events()
{
    scene::Dungeon::events();
}

La64::La64() : Game()
{
    startScene(new TailCave(this));
}

La64::~La64()
{

}
