Seashell
========

This project is a library to build a Zelda-like game in 3D like Ocarina of Time for the Nintendo 64.

Dependencies
------------

- [Cmake](https://cmake.org/)
- C++ compiler like [GCC](https://gcc.gnu.org/) that supports C++11 standard
- [Irrlicht](http://irrlicht.sourceforge.net/) >= 1.8
- [Boost](https://www.boost.org/) >= 1.65
- [OpenAL](http://www.openal.org/)
- [libsndfile](http://www.mega-nerd.com/libsndfile/)

### Debian

```bash
sudo apt-get install build-essential cmake libirrlicht-dev libboost-dev libopenal-dev libsndfile-dev
```

### Windows

I provide a [Code::Blocks](http://www.codeblocks.org/) project with the project so don't hesitate to use it as the cmake solution is problematic under Windows.

Just be sure to install correctly all dependencies (like Irrlicht) and add all libraries to Linker settings in the Code::Blocks project before building the library.

Then, you can find the static libary (`libSeashell.a`) into the bin directory. Add it to your project belong all other libraries needed.

### Other systems

Feel free to contribute ;)

Compilation and installation
----------------------------

```bash
cmake .
make
sudo make install
```

Usage
-----

See [the tutorial](doc/tutorial.md) to see how to use Seashell and create a Zelda64-like game.
