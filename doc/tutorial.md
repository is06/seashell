Tutorial
========

Set up your CMake configuration
-------------------------------

We recommand to use [CMake](https://cmake.org/) to compile your project as it is the simplier way to do it.
This is a `CMakeLists.txt` file example that you can use to build your game from seashell.

```cmake
cmake_minimum_required (VERSION 2.6)

# Project name
project (MyZelda64Game)

# Where the source files are located
file (GLOB_RECURSE source_files src/*.cpp)

# The executable that will be build from your sources
add_executable (MyZelda64Game ${source_files})

# What libraries to build
# Warning: seashell has to be the first library to link
target_link_libraries (MyZelda64Game seashell Irrlicht openal sndfile)
```

Create a game class
-------------------

We create a `include/MyGame.h` file. Our game class must inherit from `seashell::Game`.

```cpp
#ifndef MYGAME_H
#define MYGAME_H

#include <seashell/Seashell.h>

class MyGame : public seashell::Game
{
    public:
        MyGame();
        virtual ~MyGame();
};

#endif
```

In a `src/MyGame.cpp` file, the game class method implementations are as follow:

```cpp
#include <seashell/Seashell.h>
#include "MyGame.h"

MyGame::MyGame() : seashell::Game()
{
    // Here we create a scene and start it
    startScene(new SimpleScene(this, "my_map"));
}

MyGame::~MyGame() {}
```

Then we write the main program and run our game class.

```cpp
#include <seashell/Seashell.h>
#include "MyGame.h"

using namespace seashell;

int main()
{
    MyGame* game = new MyGame();
    game->run();
    delete game;

    return 0;
}
```

Build and run the game
----------------------

```bash
cmake .
make
cd bin
./MyZelda64Game
```
