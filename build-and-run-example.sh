#!/bin/sh

cmake .
make -j4
sudo make install

cd example
rm -rf CMakeFiles
cmake .
make -j4

cd bin
./example
