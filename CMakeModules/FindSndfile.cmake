find_path (
    SNDFILE_INCLUDE_DIR
        NAMES
            sndfile.h
)

find_library (
    SNDFILE_LIBRARY
        NAMES
            sndfile
)

include (FindPackageHandleStandardArgs)

find_package_handle_standard_args(
    SNDFILE DEFAULT_MSG
    SNDFILE_INCLUDE_DIR
    SNDFILE_LIBRARY
)

mark_as_advanced(SNDFILE_INCLUDE_DIR SNDFILE_LIBRARY)
