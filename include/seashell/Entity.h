#ifndef __SEASHELL_ENTITY_H__
#define __SEASHELL_ENTITY_H__

namespace seashell {
    
class Scene;

class Entity
{
    public:
        Entity(Scene* scene);
        virtual void update() = 0;
        virtual ~Entity();

    protected:
        Scene* scene;
};

}

#endif
