#ifndef __SEASHELL_HERO_API_H__
#define __SEASHELL_HERO_API_H__

#include "AbstractApi.h"
#include "../EventType.h"

namespace seashell { namespace entity { namespace world {
class Hero;
}}}

namespace seashell { namespace api {

class CollisionApi;

class HeroApi : public AbstractApi
{
    public:
        HeroApi(Scene* scene);
        ~HeroApi();

        void update();

        CollisionApi* collides(EventType when);

    private:
        entity::world::Hero* hero;
        CollisionApi* collisions;
};

}}

#endif
