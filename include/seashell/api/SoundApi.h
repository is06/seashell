#ifndef __SEASHELL_SOUND_API_H__
#define __SEASHELL_SOUND_API_H__

#include "AbstractApi.h"

#include <string>

namespace seashell { namespace api {
        
class SoundApi : public AbstractApi
{
    public:
        SoundApi(Scene* scene);
        virtual ~SoundApi();

        void update();
        void play(const std::string& name);
};

}}

#endif
