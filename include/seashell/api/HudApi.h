#ifndef __SEASHELL_HUD_API_H__
#define __SEASHELL_HUD_API_H__

#include "AbstractApi.h"

namespace seashell { namespace entity { namespace hud {
class GameplayHudButtonSection;
class GameplayHudLife;
class GameplayHudMinimap;
class GameplayHudRupees;
}}}

namespace seashell { namespace api {

class HudApi : public AbstractApi
{
    public:
        HudApi(Scene* scene);
        ~HudApi();

        void update();

    private:
        entity::hud::GameplayHudButtonSection* hudButtons;
        entity::hud::GameplayHudLife* life;
        entity::hud::GameplayHudMinimap* minimap;
        entity::hud::GameplayHudRupees* rupees;
};

}}

#endif
