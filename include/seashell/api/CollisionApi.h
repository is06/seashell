#ifndef __SEASHELL_COLLISION_API_H__
#define __SEASHELL_COLLISION_API_H__

#include "AbstractApi.h"

#include <string>

namespace seashell { namespace api {

class CollisionApi : public AbstractApi
{
    public:
        CollisionApi(Scene* scene);
        virtual ~CollisionApi();
        void update();

        bool withBoxSensor(const std::string& name);
        bool withChest(const std::string& name);
        bool withDoor(const std::string& name);
        bool withEnemy(const std::string& name);
        bool withItem(const std::string& name);
        bool withNpc(const std::string& name);
        bool withPlaneSensor(const std::string& name);
        bool withTransportable(const std::string& name);
        bool withWarper(const std::string& name);
};

}}

#endif
