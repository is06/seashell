#ifndef __SEASHELL_CONTROL_BUTTON_API_H__
#define __SEASHELL_CONTROL_BUTTON_API_H__

#include <irrlicht/irrlicht.h>

#include "AbstractApi.h"
#include "../ControlButton.h"

namespace seashell { namespace api {

class ControlButtonApi : public AbstractApi
{
    public:
        ControlButtonApi(Scene* scene);
        virtual ~ControlButtonApi();

        void update();
        void setButton(ControlButton button);
        bool isPressed();
        bool isMaintained();

    private:
        ControlButton button;
        static irr::EKEY_CODE keyboardMapping[BUTTON_COUNT];
};

}}

#endif
