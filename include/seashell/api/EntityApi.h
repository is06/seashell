#ifndef __SEASHELL_ENTITY_API_H__
#define __SEASHELL_ENTITY_API_H__

#include "AbstractApi.h"
#include "../ItemIdentifier.h"

#include <string>

namespace seashell { namespace entity { namespace world {
class BoxSensor;
class Chest;
class Door;
class PlaneSensor;
}}}

namespace seashell { namespace api {
        
class EntityApi : public AbstractApi
{
    public:
        EntityApi(Scene* scene);
        virtual ~EntityApi();

        void update();

        void addBoxSensor();
        void addChest(const std::string& name, float x, float y, float z, float direction, ItemIdentifier content);
        void addDoor();
        void addEnemy();
        void addItem();
        void addNpc();
        void addPlaneSensor();
        void addStartingPoint();
        void addTransportable();
        void addWarper();

        //entity::world::BoxSensor* boxSensor(const std::string& name);
        //entity::world::Chest* chest(const std::string& name);
        //entity::world::Door* door(const std::string& name);
        //Enemy* enemy(const std::string& name);
        //Item* item(const std::string& name);
        //Npc* npc(const std::string& name);
        //entity::world::PlaneSensor* planeSensor(const std::string& name);
        //StartingPoint* startingPoint(const std::string& name);
        //Warper* warper(const std::string& name);
};
    
}}

#endif
