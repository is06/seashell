#ifndef __SEASHELL_ABSTRACT_API_H__
#define __SEASHELL_ABSTRACT_API_H__

#include "../Seashell.h"

namespace seashell {
class Scene;
}

namespace seashell { namespace api {
        
class AbstractApi
{
    public:
        AbstractApi(Scene* scene);
        virtual void update() = 0;
        virtual ~AbstractApi();

    protected:
        Scene* scene;
};

}}

#endif
