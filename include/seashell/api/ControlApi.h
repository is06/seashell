#ifndef __SEASHELL_CONTROL_API_H__
#define __SEASHELL_CONTROL_API_H__

#include "AbstractApi.h"
#include "../ControlButton.h"
#include "../JoystickType.h"

namespace seashell { namespace api {
        
class ControlButtonApi;
class JoystickApi;

class ControlApi : public AbstractApi
{
    public:
        ControlApi(Scene* scene);
        virtual ~ControlApi();

        void update();

        ControlButtonApi* button(ControlButton which);
        JoystickApi* joystick(JoystickType which = JOYSTICK_LEFT);

    private:
        ControlButtonApi* buttonApi;
        JoystickApi* joystickApi;
};
    
}}

#endif
