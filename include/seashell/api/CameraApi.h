#ifndef __SEASHELL_CAMERA_API_H__
#define __SEASHELL_CAMERA_API_H__

#include <map>
#include <string>

#include "AbstractApi.h"

namespace seashell { namespace entity { namespace world {
class Camera;
}}}
 
namespace seashell { namespace api {
        
class CameraApi : public AbstractApi
{
    public:
        CameraApi(Scene* scene);
        ~CameraApi();

        void update();
        entity::world::Camera* get(const std::string& name);

    private:
        std::map<std::string, entity::world::Camera*> cameras;
};

}}

#endif
