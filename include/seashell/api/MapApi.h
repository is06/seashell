#ifndef __SEASHELL_MAP_API_H__
#define __SEASHELL_MAP_API_H__

#include "AbstractApi.h"

namespace seashell { namespace api {
        
class MapApi : public AbstractApi
{
    public:
        MapApi(Scene* scene);
        ~MapApi();

        void update();
};

}}

#endif
