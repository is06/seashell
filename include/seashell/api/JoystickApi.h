#ifndef __SEASHELL_JOYSTICK_API_H__
#define __SEASHELL_JOYSTICK_API_H__

#include "AbstractApi.h"
#include "../JoystickType.h"
#include "../JoystickDirection.h"

namespace seashell { namespace api {

class JoystickApi : public AbstractApi
{
    public:
        JoystickApi(Scene* scene);
        virtual ~JoystickApi();

        void update();
        void setType(JoystickType type);
        float angle();
        float force();
        JoystickDirection direction();
        bool isClicked();

    private:
        JoystickType type;
};

}}

#endif
