#ifndef __SEASHELL_MAP_H__
#define __SEASHELL_MAP_H__

#include "../Scene.h"

#include <string>

namespace seashell {
class Game;
}

namespace seashell { namespace api {
class HeroApi;
class HudApi;
}}

namespace seashell { namespace scene {

class Map : public Scene
{
    public:
        Map(Game* game, const std::string& name);
        virtual ~Map();

    protected:
        virtual void events() = 0;

        api::HeroApi* hero;
        api::HudApi* hud;
};

}}

#endif
