#ifndef __SEASHELL_DUNGEON_H__
#define __SEASHELL_DUNGEON_H__

#include <string>

#include "Map.h"

namespace seashell {
class Game;
}

namespace seashell { namespace scene {

class Dungeon : public Map
{
    public:
        Dungeon(Game* game, const std::string& name);
        virtual ~Dungeon();

    protected:
        virtual void events() = 0;
};

}}

#endif
