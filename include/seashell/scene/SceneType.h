#ifndef __SEASHELL_SCENE_TYPE_H__
#define __SEASHELL_SCENE_TYPE_H__

namespace seashell { namespace scene {
    
enum SceneType
{
    TYPE_SIMPLE,
    TYPE_MAP
};

}}

#endif
