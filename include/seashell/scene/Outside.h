#ifndef __SEASHELL_OUTSIDE_H__
#define __SEASHELL_OUTSIDE_H__

#include "Map.h"

namespace seashell {
class Game;
}

namespace seashell { namespace scene {

class Outside : public Map
{
    public:
        Outside(Game* game, const std::string& name);
        virtual ~Outside();

    protected:
        virtual void events() = 0;
};

}}

#endif
