#ifndef __SEASHELL_SIMPLE_SCENE_H__
#define __SEASHELL_SIMPLE_SCENE_H__

#include "../Scene.h"

namespace seashell {
class Game;
}

namespace seashell { namespace scene {
    
class SimpleScene : public Scene
{
    public:
        SimpleScene(Game* game, const std::string& name);
        virtual ~SimpleScene();

    protected:
        virtual void events() = 0;
};

}}

#endif
