#ifndef __SEASHELL_KEYBOARD_MANAGER_H__
#define __SEASHELL_KEYBOARD_MANAGER_H__

#include <irrlicht/irrlicht.h>

namespace seashell {
    
class KeyboardManager
{
    public:
        KeyboardManager();
        ~KeyboardManager();

        bool isKeyDown(irr::EKEY_CODE key);
        bool isKeyDownOnce(irr::EKEY_CODE key);
        bool trigger(const irr::SEvent& event);

    private:
        bool keyDown[irr::KEY_KEY_CODES_COUNT];
        bool keyOnce[irr::KEY_KEY_CODES_COUNT];
        bool anyDown;
        bool anyOnce;
};

}

#endif
