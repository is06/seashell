#ifndef __SEASHELL_MAP_SECTION_MANAGER_H__
#define __SEASHELL_MAP_SECTION_MANAGER_H__

#include <irrlicht/irrlicht.h>
#include <string>

using namespace std;

namespace seashell { namespace entity { namespace world {
class MapPart;
}}}

namespace seashell {

class Scene;

class MapSectionManager
{
    public:
        MapSectionManager(Scene* scene);
        ~MapSectionManager();

        void loadSection(const std::string& mapName, const std::string& sectionName, const irr::core::vector3df& position);

    private:
        void swapLevelMeshes();
        void unloadUnusedParts();

        Scene* scene;
        entity::world::MapPart* parts[2];
};

}

#endif
