#ifndef __SEASHELL_MAP_DATA_H__
#define __SEASHELL_MAP_DATA_H__

#include <irrlicht/irrlicht.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <map>
#include <vector>

namespace pt = boost::property_tree;

namespace seashell {
    
class MapPartData
{
    public:
        std::string typeIdentifier;
        bool solid;
        std::vector<irr::video::S3DVertex> vertices;
        std::vector<unsigned short> indices;
        std::string filePath;

        irr::core::vector3df position;
        irr::core::vector3df rotation;
        irr::core::vector3df scale;

        void addVertex(const pt::ptree::value_type& v);
        void addIndice(const pt::ptree::value_type& v);
};

class MapSectionData
{
    public:
        std::map<std::string, MapPartData> parts;

        void addPart(const pt::ptree::value_type& v);
};

class MapEntityData
{
    public:
        std::string type;
        irr::core::vector3df position;
};

class MapData
{
    public:
        std::string name;
        std::string title;
        std::string type;
        std::string music;
        std::map<std::string, MapSectionData> sections;
        std::map<std::string, MapEntityData> entities;

        void load(const std::string& filePath);

    private:
        void addSection(const pt::ptree::value_type& v);
        void addEntity(const pt::ptree::value_type& v);
};

}

#endif
