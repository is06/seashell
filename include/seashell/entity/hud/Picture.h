#ifndef __SEASHELL_PICTURE_H__
#define __SEASHELL_PICTURE_H__

#include "FlatElement.h"

#include <string>

namespace seashell {
class AssetManager;
}

namespace seashell { namespace entity { namespace hud {

class Picture : public FlatElement
{
    public:
        Picture(
            Scene* scene,
            float x,
            float y,
            float width = 32,
            float height = 32,
            const std::string& filePath = "assets/textures/default.bmp",
            bool useAlphaBlending = true
        );
        virtual ~Picture();
        virtual void render();
};

}}}

#endif
