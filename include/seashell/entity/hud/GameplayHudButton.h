#ifndef __SEASHELL_GAMEPLAY_HUD_BUTTON_H__
#define __SEASHELL_GAMEPLAY_HUD_BUTTON_H__

#include <irrlicht/irrlicht.h>
#include <string>

#include "../HudEntity.h"

namespace seashell { namespace entity { namespace hud {

using Color = irr::video::SColor;

class Picture;

class GameplayHudButton : public HudEntity
{
    public:
        GameplayHudButton(Scene* scene, float x, float y, Color color);
        ~GameplayHudButton();

        void render();
        void setItem(const std::string& name);
        void setText(const std::string& name);
        void disable();
        void enable();

    private:
        Picture* icon;
        Picture* item;
        Picture* text;
};

}}}

#endif
