#ifndef __SEASHELL_FLAT_ELEMENT_H__
#define __SEASHELL_FLAT_ELEMENT_H__

#include <irrlicht/irrlicht.h>
#include <string>

#include "../HudEntity.h"

namespace seashell { namespace entity { namespace hud {

using TextureCoord = irr::core::vector2df;
using Vector = irr::core::vector3df;
using Vertex = irr::video::S3DVertex;
using Color = irr::video::SColor;
using Texture = irr::video::ITexture;
using Material = irr::video::SMaterial;
using Size2D = irr::core::dimension2df;
using Matrix = irr::core::matrix4;

enum FlipType {
    FLIP_HORIZONTALLY,
    FLIP_VERTICALLY
};

class FlatElement : public HudEntity
{
    public:
        FlatElement(Scene* scene, float x, float y, float width, float height, bool alphaBlending = false);
        virtual void render() = 0;
        void setPosition(float x, float y);
        void setRotation(float degrees);
        void setSize(float width, float height);
        void setOpacity(float opacity);
        void setDiffuseColor(Color color);
        void setTexture(const std::string& filePath);
        void show();
        void hide();
        void flip(FlipType type);

        Vector getPosition();

    protected:
        Texture* texture;
        Material material;
        Vertex vertices[4];

    private:
        void initMaterial(bool alphaBlending);
        void initVertices(float width, float height);

        Size2D size;
        Matrix absoluteTransformation;

        bool visible;
};

}}}

#endif
