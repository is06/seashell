#ifndef __SEASHELL_TEXT_CHARACTER_H__
#define __SEASHELL_TEXT_CHARACTER_H__

#include "FlatElement.h"

namespace seashell {
class AssetManager;
class Scene;
}

namespace seashell { namespace entity { namespace hud {

class TextCharacter : public FlatElement
{
    public:
        TextCharacter(
            Scene* scene,
            int code,
            float x,
            float y,
            irr::video::SColor color
        );
        virtual ~TextCharacter();
        virtual void render();
};

}}}

#endif
