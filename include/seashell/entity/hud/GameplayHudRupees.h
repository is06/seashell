#ifndef __SEASHELL_GAMEPLAY_HUD_RUPEES_H__
#define __SEASHELL_GAMEPLAY_HUD_RUPEES_H__

#include "../HudEntity.h"

namespace seashell { namespace entity { namespace hud {

class Picture;

class GameplayHudRupees : public HudEntity
{
    public:
        GameplayHudRupees(Scene* scene);
        ~GameplayHudRupees();

        void render();

    private:
        Picture* icon;
};

}}}

#endif
