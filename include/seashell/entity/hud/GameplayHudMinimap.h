#ifndef __SEASHELL_GAMEPLAY_HUD_MINIMAP_H__
#define __SEASHELL_GAMEPLAY_HUD_MINIMAP_H__

#include "../HudEntity.h"

namespace seashell { namespace entity { namespace hud {

class Picture;

class GameplayHudMinimap : public HudEntity
{
    public:
        GameplayHudMinimap(Scene* scene);
        ~GameplayHudMinimap();

        void render();

    private:
        Picture* minimap;
};

}}}

#endif
