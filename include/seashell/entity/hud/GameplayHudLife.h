#ifndef __SEASHELL_GAMEPLAY_HUD_LIFE_H__
#define __SEASHELL_GAMEPLAY_HUD_LIFE_H__

#include <irrlicht/irrlicht.h>
#include <vector>

#include "../HudEntity.h"

namespace seashell { namespace entity { namespace hud {

class Picture;

class GameplayHudLife : public HudEntity
{
    public:
        GameplayHudLife(Scene* scene);
        ~GameplayHudLife();

        void render();
        void addLife(unsigned char value);
        void removeLife(unsigned char value);

    private:
        std::vector<Picture*> hearts;

        unsigned char life;
        unsigned char maxLife;

        irr::core::position2di position;
};

}}}

#endif
