#ifndef __SEASHELL_TEXT_H__
#define __SEASHELL_TEXT_H__

#include "../HudEntity.h"
#include "TextCharacter.h"

#include <string>
#include <vector>

namespace seashell {
class AssetManager;
class Scene;
}

namespace seashell { namespace entity {
class Timer;
}}

namespace seashell { namespace entity { namespace hud {

class Text : public HudEntity
{
    public:
        Text(Scene* scene, const std::string& text, float x, float y, unsigned int speed, bool shadow = true);
        virtual ~Text();
        virtual void render(); 

        unsigned char getLineCount();

    private:
        void initCharacters(bool shadow = true);
        void showNextCharacter();

        std::string text;

        std::vector<TextCharacter> characters;
        std::vector<TextCharacter> characterShadows;
        std::vector<TextCharacter>::iterator characterIterator;
        std::vector<TextCharacter>::iterator characterShadowIterator;

        entity::Timer* displayTimer;

        irr::core::position2df initPosition;
        irr::core::position2df currentCharacterPosition;

        unsigned int currentCharacter;
        unsigned int currentLine;
        unsigned int textLength;
        unsigned int displaySpeed;
        bool textDisplayFinished;
};

}}}

#endif
