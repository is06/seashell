#ifndef __SEASHELL_GAMEPLAY_HUD_BUTTON_SECTION_H__
#define __SEASHELL_GAMEPLAY_HUD_BUTTON_SECTION_H__

#include "../HudEntity.h"

namespace seashell { namespace entity { namespace hud {

class GameplayHudButton;

class GameplayHudButtonSection : public HudEntity
{
    public:
        GameplayHudButtonSection(Scene* scene);
        void render();
        ~GameplayHudButtonSection();

    private:
        GameplayHudButton* actionButton;
        GameplayHudButton* swordButton;
        GameplayHudButton* item1Button;
        GameplayHudButton* item2Button;
};

}}}

#endif
