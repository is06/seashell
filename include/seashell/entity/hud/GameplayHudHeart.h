#ifndef __SEASHELL_GAMEPLAY_HUD_HEART_H__
#define __SEASHELL_GAMEPLAY_HUD_HEART_H__

#include "../HudEntity.h"

#include <irrlicht/irrlicht.h>

namespace seashell { namespace entity { namespace hud {

class Picture;

class GameplayHudHeart : public HudEntity
{
    public:
        GameplayHudHeart(Scene* scene, float x, float y, video::SColor color);
        ~GameplayHudHeart();

        void render();

    private:
        Picture* icon;

};

}}}

#endif
