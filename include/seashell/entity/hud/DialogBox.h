#ifndef __SEASHELL_DIALOG_BOX_H__
#define __SEASHELL_DIALOG_BOX_H__

#include <string>
#include <map>
#include "../HudEntity.h"

namespace seashell { namespace entity { namespace hud {

class Text;
class Picture;

enum DialogBoxType {
    DIALOG_BOX_TYPE_NONE,
    DIALOG_BOX_TYPE_NORMAL,
    DIALOG_BOX_TYPE_ITEM,
    DIALOG_BOX_TYPE_SONG,
    DIALOG_BOX_TYPE_SIGN
};

enum DialogBoxPosition {
    DIALOG_BOX_POSITION_TOP,
    DIALOG_BOX_POSITION_MIDDLE,
    DIALOG_BOX_POSITION_BOTTOM
};

class DialogBox : public HudEntity
{
    public:
        DialogBox(Scene* scene, DialogBoxType type, DialogBoxPosition position, std::string text);
        virtual ~DialogBox();
        virtual void render();

    private:
        Text* dialog;
        Picture* leftBox;
        Picture* rightBox;
};

}}}

#endif
