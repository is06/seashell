#ifndef __SEASHELL_CAMERA_H__
#define __SEASHELL_CAMERA_H__

#include "../WorldEntity.h"

#include <irrlicht/irrlicht.h>

namespace seashell {
class Scene;
}

namespace seashell { namespace entity { namespace world {

class Camera : public WorldEntity
{
    public:
        Camera(Scene* scene);
        virtual void update() = 0;
        virtual ~Camera();

    private:
        Scene* scene;
        irr::scene::ICameraSceneNode* cameraNode;
        bool controlable;
};

}}}

#endif
