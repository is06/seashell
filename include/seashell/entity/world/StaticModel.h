#ifndef __SEASHELL_STATIC_MODEL_H__
#define __SEASHELL_STATIC_MODEL_H__

#include <irrlicht/irrlicht.h>

#include "ModelEntity.h"

namespace seashell { namespace entity { namespace world {

class StaticModel : public ModelEntity
{
    public:
        StaticModel(Scene* scene);
        virtual void update() = 0;
        virtual ~StaticModel();

        void createNode(
            const irr::core::vector3df& position,
            const irr::core::vector3df& rotation,
            const irr::core::vector3df& scale
        );

    protected:
        irr::scene::IMeshSceneNode* node;
};

}}}

#endif
