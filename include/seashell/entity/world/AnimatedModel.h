#ifndef __SEASHELL_ANIMATED_MODEL_H__
#define __SEASHELL_ANIMATED_MODEL_H__

#include <irrlicht/irrlicht.h>

#include "ModelEntity.h"

namespace seashell { namespace entity { namespace world {

class AnimatedModel : public ModelEntity
{
    public:
        AnimatedModel(Scene* scene);
        ~AnimatedModel();

        void update();
        void createNode(const irr::core::vector3df& position);

    private:
        irr::scene::IAnimatedMeshSceneNode* node;
};

}}}

#endif
