#ifndef __SEASHELL_CHARACTER_H__
#define __SEASHELL_CHARACTER_H__

#include "AnimatedModel.h"

namespace seashell { namespace entity { namespace world {

class Character : public AnimatedModel
{
    public:
        Character(Scene* scene);
        ~Character();

        void update();
};

}}}

#endif
