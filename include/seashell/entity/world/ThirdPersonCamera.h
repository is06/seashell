#ifndef __SEASHELL_THIRD_PERSON_CAMERA_H__
#define __SEASHELL_THIRD_PERSON_CAMERA_H__

#include "Camera.h"

namespace seashell { namespace entity { namespace world {
    
class ThirdPersonCamera : public Camera
{
    public:
        ThirdPersonCamera(Scene* scene);
        virtual void update();
        virtual ~ThirdPersonCamera();
};

}}}

#endif
