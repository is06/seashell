#ifndef __SEASHELL_DOOR_H__
#define __SEASHELL_DOOR_H__

#include "../WorldEntity.h"

namespace seashell { namespace entity { namespace world {

class Door : public WorldEntity
{
    public:
        Door(Scene* scene);
        virtual void update();
        virtual ~Door();
};

}}}

#endif
