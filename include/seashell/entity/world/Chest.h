#ifndef __SEASHELL_CHEST_H__
#define __SEASHELL_CHEST_H__

#include "../WorldEntity.h"

namespace seashell { namespace entity { namespace world {

class Chest : public WorldEntity
{
    public:
        Chest(Scene* scene);
        virtual void update();
        virtual ~Chest();

        bool isOpen();
};

}}}

#endif
