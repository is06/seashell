#ifndef __SEASHELL_PLANE_SENSOR_H__
#define __SEASHELL_PLANE_SENSOR_H__

#include "Sensor.h"

namespace seashell { namespace entity { namespace world {
    
class PlaneSensor : public Sensor
{
    public:
        PlaneSensor(Scene* scene);
        virtual void update();
        virtual ~PlaneSensor();
};

}}}

#endif
