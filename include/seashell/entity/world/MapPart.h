#ifndef __SEASHELL_MAP_PART_H__
#define __SEASHELL_MAP_PART_H__

#include "StaticModel.h"

#include <string>

namespace seashell {
class MapPartData;
}

namespace seashell { namespace entity { namespace world {

class MapPart : public StaticModel
{
    public:
        MapPart(Scene* scene);
        virtual ~MapPart();

        virtual void update();
        void loadData(const MapPartData& data);
        void loadFromObjectFile(const std::string& filePath);
};

}}}

#endif
