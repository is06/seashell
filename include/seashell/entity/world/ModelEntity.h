#ifndef __SEASHELL_MODEL_ENTITY_H__
#define __SEASHELL_MODEL_ENTITY_H__

#include <irrlicht/irrlicht.h>
#include <vector>
#include <string>

#include "../WorldEntity.h"

using namespace std;

namespace seashell { namespace entity { namespace world {

class ModelEntity : public WorldEntity
{
    public:
        ModelEntity(Scene* scene);
        virtual void update() = 0;
        virtual ~ModelEntity();

        void loadMeshFromFile(const std::string& filePath);
        void loadMeshFromVertices(
            const std::vector<irr::video::S3DVertex>& vertices,
            const std::vector<unsigned short>& indices
        );

    protected:
        irr::scene::IMesh* mesh;
};

}}}

#endif
