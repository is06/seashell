#ifndef __SEASHELL_BOX_SENSOR_H__
#define __SEASHELL_BOX_SENSOR_H__

#include "Sensor.h"

namespace seashell { namespace entity { namespace world {

class BoxSensor : public Sensor
{
    public:
        BoxSensor(Scene* scene);
        virtual void update();
        virtual ~BoxSensor();
};

}}}

#endif
