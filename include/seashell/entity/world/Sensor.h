#ifndef __SEASHELL_SENSOR_H__
#define __SEASHELL_SENSOR_H__

#include "../WorldEntity.h"

namespace seashell { namespace entity { namespace world {

class Sensor : public WorldEntity
{
    public:
        Sensor(Scene* scene);
        virtual void update() = 0;
        virtual ~Sensor();
};

}}}

#endif
