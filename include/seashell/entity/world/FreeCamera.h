#ifndef __SEASHELL_FREE_CAMERA_H__
#define __SEASHELL_FREE_CAMERA_H__

#include "Camera.h"

namespace seashell { namespace entity { namespace world {

class FreeCamera : public Camera
{
    public:
        FreeCamera(Scene* scene);
        ~FreeCamera();

        virtual void update();
};

}}}

#endif
