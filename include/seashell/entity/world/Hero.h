#ifndef __SEASHELL_HERO_H__
#define __SEASHELL_HERO_H__

#include "Character.h"

namespace seashell { namespace entity { namespace world {

class Camera;

class Hero : public Character
{
    public:
        Hero(Scene* scene, Camera* linkedCamera);
        ~Hero();

        void update();

    private:
        Camera* linkedCamera;
        bool controllable;
};

}}}

#endif
