#ifndef __SEASHELL_WORLD_ENTITY_H__
#define __SEASHELL_WORLD_ENTITY_H__

#include "../Entity.h"

namespace seashell { namespace entity {

class WorldEntity : public Entity
{
    public:
        WorldEntity(Scene* scene);
        virtual void update() = 0;
        virtual ~WorldEntity();
};
    
}}

#endif
