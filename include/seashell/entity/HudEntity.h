#ifndef __SEASHELL_HUD_ENTITY_H__
#define __SEASHELL_HUD_ENTITY_H__

#include "../Entity.h"

namespace seashell { namespace entity {

class HudEntity : public Entity
{
    public:
        HudEntity(Scene* scene);
        virtual void render() = 0;
        virtual void update();
        virtual ~HudEntity();
};

}}

#endif
