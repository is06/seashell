#ifndef __SEASHELL_TIMER_H__
#define __SEASHELL_TIMER_H__

#include <boost/function.hpp>

#include "../Entity.h"

namespace seashell { namespace entity {

class Timer : public Entity
{
    public:
        Timer(Scene* scene, float duration, boost::function<void()> callback, int loopCount = 1);
        virtual ~Timer();
        virtual void update();

        inline void start() { running = true; }
        inline void stop() { running = false; }
        inline void reset() { currentTime = 0.0f; }

    private:
        boost::function<void()> callback;
        bool running;
        float currentTime;
        float endTime;
        int loopCount;
        int loopIndex;
};

}}

#endif
