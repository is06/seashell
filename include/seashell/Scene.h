#ifndef __SEASHELL_SCENE_H__
#define __SEASHELL_SCENE_H__

#include <map>
#include <string>

#include "scene/SceneType.h"
#include "FadeColor.h"
#include "entity/hud/Text.h"

namespace seashell { namespace api {

class MapApi;
class EntityApi;
class CameraApi;
class SoundApi;
class ControlApi;

}}

namespace seashell { namespace entity {
class HudEntity;
}}

namespace seashell { namespace entity { namespace world {
class MapPart;
class Camera;
}}}

namespace seashell { namespace entity { namespace hud {
class Picture;
class DialogBox;
}}}

namespace seashell {

class Game;
class MapSectionData;

class Scene
{
    public:
        Scene(Game* game, const std::string& name);
        virtual ~Scene();

        virtual void events() = 0;
        void hudRender();

        scene::SceneType getSceneType();
        Game* getGame();
        entity::world::Camera* getMainCamera();

    protected:
        api::MapApi* map;
        api::EntityApi* entity;
        api::CameraApi* camera;
        api::SoundApi* sound;
        api::ControlApi* control;

        void fadeIn(float speed = 1.5f, FadeColor color = FADE_COLOR_BLACK);
        void fadeOut(float speed = 3.0f, FadeColor color = FADE_COLOR_BLACK);

    private:
        void loadFromData();
        void loadSection(const MapSectionData& section);

        Game* game;
        entity::hud::Picture* dummy;

        std::map<std::string, entity::HudEntity*> hudEntities;
        std::map<std::string, entity::world::MapPart*> mapParts;

        std::string name;
        scene::SceneType type;
        float startTime;
        float currentTime;
        float speedFactor;

        entity::hud::DialogBox* tempDialog;
};

}

#endif
