#ifndef __SEASHELL_JOYSTICK_TYPE_H__
#define __SEASHELL_JOYSTICK_TYPE_H__

namespace seashell {
    
enum JoystickType
{
    JOYSTICK_LEFT,
    JOYSTICK_RIGHT
};

}

#endif
