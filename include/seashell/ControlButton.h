#ifndef __SEASHELL_CONTROL_BUTTON_H__
#define __SEASHELL_CONTROL_BUTTON_H__

namespace seashell {

enum ControlButton
{
    BUTTON_ACTION,
    BUTTON_SWORD,
    BUTTON_ITEM_1,
    BUTTON_ITEM_2,
    BUTTON_START,
    BUTTON_Z_TRIGGER,
    BUTTON_L_TRIGGER,
    BUTTON_R_TRIGGER,

    BUTTON_COUNT
};

}

#endif