#ifndef __SEASHELL_GAMEPAD_MANAGER_H__
#define __SEASHELL_GAMEPAD_MANAGER_H__

#include <irrlicht/irrlicht.h>

namespace seashell {
    
class GamepadManager
{
    public:
        GamepadManager();
        ~GamepadManager();

        float getLeftJoystickForce();
        float getLeftJoystickAngle();
        float getRightJoystickForce();
        float getRightJoystickAngle();
        unsigned int getPressedButtons();

        bool trigger(const irr::SEvent& event);

    private:
        void setButtonStates(const irr::SEvent& event);
        void setJoystickStates(const irr::SEvent& event);

        float leftJoystickAngle;
        float rightJoystickAngle;
        unsigned char leftJoystickForce;
        unsigned char rightJoystickForce;
        unsigned short buttonStates;
        unsigned int povValue;
};

}

#endif
