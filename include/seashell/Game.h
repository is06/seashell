#ifndef __SEASHELL_GAME_H__
#define __SEASHELL_GAME_H__

#include <irrlicht/irrlicht.h>
#include <string>

#include "AssetManager.h"
#include "AudioManager.h"
#include "EventManager.h"
#include "Scene.h"

namespace seashell {
    
class Scene;

class Game
{
    public:
        Game();
        void run();
        void setTitle(const std::wstring& title);
        ~Game();

        irr::video::IVideoDriver* getVideoDriver();
        irr::scene::ISceneManager* getSceneManager();
        AssetManager* getAssetManager();
        AudioManager* getAudioManager();
        EventManager* getEventManager();

        float getSpeedFactor();

    protected:
        void startScene(Scene* scene);

    private:
        void initIrrlicht();

        irr::IrrlichtDevice* device;
        irr::video::IVideoDriver* videoDriver;
        irr::scene::ISceneManager* sceneManager;

        AssetManager* assetManager;
        AudioManager* audioManager;
        EventManager* eventManager;
        Scene* currentScene;

        float speedFactor;
};

}

#endif
