#ifndef __SEASHELL_EVENT_MANAGER_H__
#define __SEASHELL_EVENT_MANAGER_H__

#include <irrlicht/irrlicht.h>

#include "GamepadManager.h"
#include "KeyboardManager.h"

namespace seashell {
    
class EventManager : public irr::IEventReceiver
{
    public:
        EventManager();
        ~EventManager();
        bool OnEvent(const irr::SEvent& event);

        GamepadManager* getGamepadManager();
        KeyboardManager* getKeyboardManager();

    private:
        GamepadManager* gamepad;
        KeyboardManager* keyboard;
};

}

#endif
