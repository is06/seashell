#ifndef __SEASHELL_FADE_COLOR_H__
#define __SEASHELL_FADE_COLOR_H__

namespace seashell {
    
enum FadeColor
{
    FADE_COLOR_BLACK,
    FADE_COLOR_WHITE
};

}

#endif
