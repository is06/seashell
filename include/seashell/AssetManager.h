#ifndef __SEASHELL_ASSET_MANAGER_H__
#define __SEASHELL_ASSET_MANAGER_H__

#include <irrlicht/irrlicht.h>
#include <string>

namespace seashell {
    
class Game;

class AssetManager
{
    public:
        AssetManager(Game* game);
        ~AssetManager();

        irr::scene::IMesh* loadMesh(const std::string& path);
        irr::video::ITexture* loadTexture(const std::string& path);

    private:
        Game* game;
};

}

#endif