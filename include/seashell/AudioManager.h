#ifndef __SEASHELL_AUDIO_MANAGER_H__
#define __SEASHELL_AUDIO_MANAGER_H__

#include <AL/al.h>
#include <AL/alc.h>
#include <string>
#include <vector>

namespace seashell {
    
class AudioManager
{
    public:
        AudioManager();
        ~AudioManager();

        void update();

        void playSound(const std::string& name);

    private:
        ALuint getBufferFromFile(const std::string& filePath);

        ALuint mainSource;
        std::vector<ALuint> mainBuffers;

        ALCdevice* device;
        ALCcontext* context;
};

}

#endif
