#ifndef __SEASHELL_ITEM_IDENTIFIER_H__
#define __SEASHELL_ITEM_IDENTIFIER_H__

namespace seashell {
    
enum ItemIdentifier
{
    ITEM_SEASHELL,
    ITEM_HEART,
    ITEM_PIECE_OF_HEART,
    ITEM_RUPEE,
    ITEM_5_RUPEES,
    ITEM_20_RUPEES,
    ITEM_50_RUPEES,
    ITEM_100_RUPEES,
    ITEM_200_RUPEES
};

}

#endif
