#ifndef __SEASHELL_EVENT_TYPE_H__
#define __SEASHELL_EVENT_TYPE_H__

namespace seashell {
    
enum EventType
{
    EVENT_TYPE_ONCE,
    EVENT_TYPE_ALWAYS
};

}

#endif
