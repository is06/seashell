#include "../../include/seashell/entity/Timer.h"
#include "../../include/seashell/Scene.h"
#include "../../include/seashell/Game.h"

namespace seashell { namespace entity {

Timer::Timer(Scene* scene, float duration, boost::function<void()> callback, int loopCount) : Entity(scene)
{
    running = true;
    currentTime = 0.0f;
    endTime = duration;
    this->callback = callback;
    this->loopIndex = 0;
    this->loopCount = loopCount;
}

void Timer::update()
{
    if (running && loopIndex < loopCount) {
        currentTime += scene->getGame()->getSpeedFactor();

        if (currentTime >= endTime) {
            callback();
            
            if (loopCount > 0) {
                loopIndex++;
                reset();
            } else {
                stop();
            }
        }
    }
}

Timer::~Timer()
{

}

}}