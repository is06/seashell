#include "../../../include/seashell/entity/world/ModelEntity.h"
#include "../../../include/seashell/Game.h"

using namespace std;

namespace seashell { namespace entity { namespace world {

ModelEntity::ModelEntity(Scene* scene) : WorldEntity(scene)
{
    mesh = nullptr;
}

void ModelEntity::update()
{
    Entity::update();
}

void ModelEntity::loadMeshFromFile(const std::string& filePath)
{
    mesh = scene->getGame()->getAssetManager()->loadMesh(filePath);
}

void ModelEntity::loadMeshFromVertices(
    const std::vector<irr::video::S3DVertex>& vertices,
    const std::vector<unsigned short>& indices
) {
    irr::scene::SMesh* meshObject = new irr::scene::SMesh();
    irr::scene::SMeshBuffer* meshBuffer = new irr::scene::SMeshBuffer();
    meshBuffer->append(&vertices[0], vertices.size(), &indices[0], indices.size());
    meshObject->addMeshBuffer(meshBuffer);
    mesh = meshObject;
    meshBuffer->drop();
    delete meshBuffer;
}

ModelEntity::~ModelEntity()
{

}

}}}
