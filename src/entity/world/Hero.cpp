#include "../../../include/seashell/entity/world/Hero.h"

namespace seashell { namespace entity { namespace world {

Hero::Hero(Scene* scene, Camera* linkedCamera) : Character(scene)
{
    this->linkedCamera = linkedCamera;
    controllable = true;
}

void Hero::update()
{
    Character::update();
}

Hero::~Hero()
{

}

}}}
