#include "../../../include/seashell/entity/world/AnimatedModel.h"
#include "../../../include/seashell/entity/world/ModelEntity.h"
#include "../../../include/seashell/Scene.h"
#include "../../../include/seashell/Game.h"

namespace seashell { namespace entity { namespace world {

AnimatedModel::AnimatedModel(Scene* scene) : ModelEntity(scene)
{

}

void AnimatedModel::update()
{
    ModelEntity::update();
}

void AnimatedModel::createNode(const irr::core::vector3df& position)
{
    if (mesh == nullptr) {
        throw std::runtime_error("Unable to retrieve mesh for this static model");
    }

    node = scene->getGame()->getSceneManager()->addAnimatedMeshSceneNode((irr::scene::IAnimatedMesh*) mesh);
    node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    node->setMaterialFlag(irr::video::EMF_BILINEAR_FILTER, true);
    node->setPosition(position);
}

AnimatedModel::~AnimatedModel()
{
    if (node) {
        node->remove();
        node = nullptr;
    }
}

}}}
