#include "../../../include/seashell/entity/world/Camera.h"
#include "../../../include/seashell/Game.h"
#include "../../../include/seashell/Scene.h"

namespace seashell { namespace entity { namespace world {

Camera::Camera(Scene* scene) : WorldEntity(scene)
{
    this->scene = scene;
    controlable = true;

    cameraNode = scene->getGame()->getSceneManager()->addCameraSceneNode(0, irr::core::vector3df(0, 0, 0));
    cameraNode->bindTargetAndRotation(true);
    cameraNode->setNearValue(0.1f);
}

void Camera::update()
{

}

Camera::~Camera()
{
    cameraNode->remove();
}

}}}
