#include "../../../include/seashell/entity/world/FreeCamera.h"

namespace seashell { namespace entity { namespace world {

FreeCamera::FreeCamera(Scene* scene) : Camera(scene)
{

}

void FreeCamera::update()
{
    Camera::update();
}

FreeCamera::~FreeCamera()
{

}

}}}
