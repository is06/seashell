#include "../../../include/seashell/entity/world/Sensor.h"

namespace seashell { namespace entity { namespace world {
    
Sensor::Sensor(Scene* scene) : WorldEntity(scene)
{

}

void Sensor::update()
{
    WorldEntity::update();
}

Sensor::~Sensor()
{

}

}}}
