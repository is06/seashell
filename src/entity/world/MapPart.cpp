#include "../../../include/seashell/entity/world/MapPart.h"
#include "../../../include/seashell/MapData.h"

namespace seashell { namespace entity { namespace world {

MapPart::MapPart(Scene* scene) : StaticModel(scene)
{

}

void MapPart::update()
{
    StaticModel::update();
}

void MapPart::loadData(const MapPartData& data)
{
    loadMeshFromVertices(data.vertices, data.indices);
}

MapPart::~MapPart()
{

}

}}}
