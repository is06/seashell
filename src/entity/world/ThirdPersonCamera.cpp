#include "../../../include/seashell/entity/world/ThirdPersonCamera.h"

namespace seashell { namespace entity { namespace world {

ThirdPersonCamera::ThirdPersonCamera(Scene* scene) : Camera(scene)
{

}

void ThirdPersonCamera::update()
{
    Camera::update();
}

ThirdPersonCamera::~ThirdPersonCamera()
{

}

}}}
