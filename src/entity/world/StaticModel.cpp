#include "../../../include/seashell/entity/world/StaticModel.h"
#include "../../../include/seashell/Game.h"

namespace seashell { namespace entity { namespace world {

StaticModel::StaticModel(Scene* scene) : ModelEntity(scene)
{

}

void StaticModel::update()
{
    ModelEntity::update();
}

void StaticModel::createNode(
    const irr::core::vector3df& position,
    const irr::core::vector3df& rotation,
    const irr::core::vector3df& scale
) {
    if (mesh == nullptr) {
        throw std::runtime_error("Unable to retrieve mesh for this static model");
    }

    node = scene->getGame()->getSceneManager()->addMeshSceneNode(mesh);
    
    node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    node->setMaterialFlag(irr::video::EMF_BILINEAR_FILTER, true);

    // Debug purposes
    node->setMaterialFlag(irr::video::EMF_WIREFRAME, true);
    node->getMaterial(0).DiffuseColor = irr::video::SColor(255, 255, 0, 0);
    node->setDebugDataVisible(1);
    
    node->setPosition(position);
    node->setRotation(rotation);
    node->setScale(scale);
}

StaticModel::~StaticModel()
{
    if (node) {
        node->remove();
        node = nullptr;
    }
}

}}}
