#include "../../../include/seashell/entity/world/Chest.h"

namespace seashell { namespace entity { namespace world {

Chest::Chest(Scene* scene) : WorldEntity(scene)
{

}

void Chest::update()
{
    Entity::update();
}

bool Chest::isOpen()
{
    return false;
}

Chest::~Chest()
{

}

}}}
