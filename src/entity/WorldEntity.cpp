#include "../../include/seashell/entity/WorldEntity.h"

namespace seashell { namespace entity {

WorldEntity::WorldEntity(Scene* scene) : Entity(scene)
{

}

void WorldEntity::update()
{
    Entity::update();
}

WorldEntity::~WorldEntity()
{

}

}}
