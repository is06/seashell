#include "../../include/seashell/entity/HudEntity.h"

namespace seashell { namespace entity {

//! Constructor
HudEntity::HudEntity(Scene* scene) : Entity(scene)
{

}

//! Called on every frame to render the entity on the screen
void HudEntity::render()
{

}

//! Called on every frame to update state of the entity
void HudEntity::update()
{

}

//! Destructor
HudEntity::~HudEntity()
{

}

}}
