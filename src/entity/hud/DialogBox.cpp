#include "../../../include/seashell/entity/hud/DialogBox.h"
#include "../../../include/seashell/entity/hud/Picture.h"
#include "../../../include/seashell/Scene.h"

namespace seashell { namespace entity { namespace hud {

DialogBox::DialogBox(Scene* scene, DialogBoxType type, DialogBoxPosition position, std::string text) : HudEntity(scene)
{
    float boxYPosition = 0.0f;
    if (position == DIALOG_BOX_POSITION_TOP) {
        boxYPosition = 112.0f;
    }
    if (position == DIALOG_BOX_POSITION_BOTTOM) {
        boxYPosition = -112.0f;
    }

    leftBox = new Picture(scene, -128, boxYPosition, 256, 128, "hud/gameplay/dialog_box.png");
    leftBox->setDiffuseColor(Color(255, 0, 0, 0));
    leftBox->setOpacity(0.5f);

    rightBox = new Picture(scene, 128, boxYPosition, 256, 128, "hud/gameplay/dialog_box.png");
    rightBox->flip(FLIP_HORIZONTALLY);
    rightBox->setDiffuseColor(Color(255, 0, 0, 0));
    rightBox->setOpacity(0.5f);
    
    dialog = new Text(scene, text, -192, -76, 10);
}

void DialogBox::render()
{
    leftBox->render();
    rightBox->render();
    dialog->render();
}

DialogBox::~DialogBox()
{
    delete leftBox;
    delete rightBox;
    delete dialog;
}

}}}
