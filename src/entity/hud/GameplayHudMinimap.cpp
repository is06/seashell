#include "../../../include/seashell/Game.h"
#include "../../../include/seashell/entity/hud/GameplayHudMinimap.h"
#include "../../../include/seashell/entity/hud/Picture.h"

namespace seashell { namespace entity { namespace hud {

GameplayHudMinimap::GameplayHudMinimap(Scene* scene) : HudEntity(scene)
{
    minimap = new Picture(scene, 180, -112, 192, 192, "hud/minimaps/test.png");
    minimap->setDiffuseColor(irr::video::SColor(255, 96, 240, 240));
}

void GameplayHudMinimap::render()
{
    HudEntity::render();

    minimap->render();
}

GameplayHudMinimap::~GameplayHudMinimap()
{
    delete minimap;
}

}}}
