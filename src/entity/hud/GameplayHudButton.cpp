#include "../../../include/seashell/Game.h"
#include "../../../include/seashell/entity/hud/GameplayHudButton.h"
#include "../../../include/seashell/entity/hud/Picture.h"

namespace seashell { namespace entity { namespace hud {

GameplayHudButton::GameplayHudButton(Scene* scene, float x, float y, irr::video::SColor color) : HudEntity(scene)
{
    icon = new Picture(scene, x, y, 60, 60, "hud/gameplay/buttons.png");
    icon->setDiffuseColor(color);

    item = nullptr;
    text = nullptr;
}

void GameplayHudButton::render()
{
    HudEntity::render();

    icon->render();
    if (item != nullptr) {
        item->render();
    }
    if (text != nullptr) {
        text->render();
    }
}

void GameplayHudButton::setItem(const std::string& name)
{
    item = new Picture(
        scene,
        icon->getPosition().X,
        icon->getPosition().Y,
        64,
        64,
        "hud/gameplay/items/" + name + ".png"
    );
}

void GameplayHudButton::setText(const std::string& name)
{
    text = new Picture(
        scene,
        icon->getPosition().X,
        icon->getPosition().Y,
        96,
        32,
        "hud/gameplay/actions/en/" + name + ".png"
    );
}

void GameplayHudButton::disable()
{
    icon->setOpacity(0.25f);
}

void GameplayHudButton::enable()
{
    icon->setOpacity(1.0f);
}

GameplayHudButton::~GameplayHudButton()
{
    delete icon;
    if (item != nullptr) {
        delete item;
    }
    if (text != nullptr) {
        delete text;
    }
}

}}}
