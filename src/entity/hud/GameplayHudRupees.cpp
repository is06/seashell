#include "../../../include/seashell/Game.h"
#include "../../../include/seashell/entity/hud/GameplayHudRupees.h"
#include "../../../include/seashell/entity/hud/Picture.h"

namespace seashell { namespace entity { namespace hud {

using Color = irr::video::SColor;

GameplayHudRupees::GameplayHudRupees(Scene* scene) : HudEntity(scene)
{
    icon = new Picture(scene, -256, -192, 32, 32, "hud/gameplay/rupees.png");
    icon->setDiffuseColor(Color(255, 200, 255, 100));
}

void GameplayHudRupees::render()
{
    HudEntity::render();

    icon->render();
}

GameplayHudRupees::~GameplayHudRupees()
{
    delete icon;
}

}}}
