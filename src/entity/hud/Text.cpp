#include "../../../include/seashell/entity/hud/Text.h"
#include "../../../include/seashell/entity/Timer.h"

#include <boost/bind.hpp>

namespace seashell { namespace entity { namespace hud {

//! Constructor
/**
 * \param Scene* scene where to place the flat element
 * \param std::wstring text the text to display
 * \param float x X position on the screen
 * \param float y Y position on the screen
 */
Text::Text(Scene* scene, const std::string& text, float x, float y, unsigned int speed, bool shadow) : HudEntity(scene)
{
    this->text = text;
    textLength = text.size();
    displaySpeed = speed;
    initPosition = currentCharacterPosition = irr::core::position2df(x, y);
    currentCharacter = 0;
    currentLine = 0;
    displayTimer = nullptr;

    initCharacters(shadow);

    if (displaySpeed > 0) {
        displayTimer = new Timer(scene, 0.5f, boost::bind(&Text::showNextCharacter, this), textLength);
    }
}

//! Create characters in the vector with correct position
void Text::initCharacters(bool shadow)
{
    irr::video::SColor color = irr::video::SColor(255, 255, 255, 255); // TODO: remove this hard coded value
    const char* characterSequence = text.c_str();
    bool characterIsEscaped = false;

    for (unsigned int i = 0; i < textLength; i++) {        
        if (characterSequence[i] == '\\') {
            characterIsEscaped = true;
            continue;
        }

        if (characterIsEscaped) {
            if (characterSequence[i] == 'n') {
                currentLine++;
                currentCharacterPosition.X = initPosition.X;
                currentCharacterPosition.Y -= 24; // TODO: remove this hard coded value
                characterIsEscaped = false;
            }
            continue;
        }

        if (shadow) {
            characterShadows.push_back(TextCharacter(
                scene,
                characterSequence[i],
                currentCharacterPosition.X + 2.0f,
                currentCharacterPosition.Y - 2.0f,
                Color(255, 0, 0, 0) // TODO: remove this hard coded value
            ));
        }

        characters.push_back(TextCharacter(
            scene,
            characterSequence[i],
            currentCharacterPosition.X,
            currentCharacterPosition.Y,
            color
        ));

        currentCharacterPosition.X += 16; // TODO: remove this hard coded value
    }
}

//! Called on every frame render the text on the screen
void Text::render()
{
    if (displayTimer) {
        displayTimer->update();
    }
    for (characterShadowIterator = characterShadows.begin(); characterShadowIterator != characterShadows.end(); characterShadowIterator++) {
        characterShadowIterator->render();
    }
    for (characterIterator = characters.begin(); characterIterator != characters.end(); characterIterator++) {
        characterIterator->render();
    }
}

void Text::showNextCharacter()
{
    if (currentCharacter < textLength) {
        characters[currentCharacter].show();
        characterShadows[currentCharacter].show();
    }
    if (!textDisplayFinished && currentCharacter >= (textLength - 1)) {
        textDisplayFinished = true;
    }
    currentCharacter++;
}

unsigned char Text::getLineCount()
{
    
}

//! Destructor
Text::~Text()
{

}

}}}
