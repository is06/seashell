#include "../../../include/seashell/entity/hud/FlatElement.h"
#include "../../../include/seashell/Game.h"

using namespace std;

namespace seashell { namespace entity { namespace hud {

const float FLAT_ELEMENT_FAR_VALUE = 0.1f;
const float FLAT_ELEMENT_COEFF = 3303.38f;
const short unsigned int FLAT_ELEMENT_INDICES[6] = {2, 1, 3, 0, 1, 2};
const Matrix FLAT_ELEMENT_WORLD_MATRIX;

//! Constructor
/**
 * \param Scene* scene where to place the flat element
 * \param float x X position on the screen
 * \param float y Y position on the screen
 * \param float width Width of the flat element
 * \param float height Height of the flat element
 * \param bool alphaBlending If the Alpha Blending mode is active
 */
FlatElement::FlatElement(Scene* scene, float x, float y, float width, float height, bool alphaBlending) : HudEntity(scene)
{
    absoluteTransformation = Matrix();
    absoluteTransformation.makeIdentity();

    texture = NULL;
    visible = true;

    size = Size2D(width, height);
    setPosition(x, y);

    initVertices(size.Width, size.Height);
    initMaterial(alphaBlending);
}

//! Called on every frame render the flat element on the screen
void FlatElement::render()
{
    HudEntity::render();

    if (visible) {
        if (texture) {
            material.setTexture(0, texture);
        }

        for (unsigned int i = 0; i <= 3; i++) {
            vertices[i].Color = material.DiffuseColor;
        }

        scene->getGame()->getVideoDriver()->setMaterial(material);
        scene->getGame()->getVideoDriver()->setTransform(irr::video::ETS_VIEW, absoluteTransformation);
        scene->getGame()->getVideoDriver()->drawIndexedTriangleList(vertices, 4, FLAT_ELEMENT_INDICES, 2);
        scene->getGame()->getVideoDriver()->setTransform(irr::video::ETS_WORLD, FLAT_ELEMENT_WORLD_MATRIX);
    }
}

//! Init the material of the flat element
void FlatElement::initMaterial(bool alphaBlending)
{
    material.Lighting = false;

    if (alphaBlending) {
        material.MaterialType = irr::video::EMT_ONETEXTURE_BLEND;
        material.MaterialTypeParam = irr::video::pack_textureBlendFunc(
            irr::video::EBF_SRC_ALPHA,
            irr::video::EBF_ONE_MINUS_SRC_ALPHA,
            irr::video::EMFN_MODULATE_1X,
            irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR
        );
    } else {
        material.MaterialType = irr::video::EMT_TRANSPARENT_VERTEX_ALPHA;
    }

    material.setFlag(irr::video::EMF_BILINEAR_FILTER, true);
    material.setTexture(0, NULL);

    material.TextureLayer[0].TextureWrapU = irr::video::ETC_CLAMP_TO_EDGE;
    material.TextureLayer[0].TextureWrapV = irr::video::ETC_CLAMP_TO_EDGE;
}

//! Init the vertices of the flat element
void FlatElement::initVertices(float width, float height)
{
    vertices[0] = Vertex(
        Vector(width / 2 / FLAT_ELEMENT_COEFF * -1, height / 2 / FLAT_ELEMENT_COEFF, FLAT_ELEMENT_FAR_VALUE), // position
        Vector(1.0f, 1.0f, 1.0f), // normal
        material.DiffuseColor, // color
        TextureCoord(0.0f, 0.0f) // texture coords
    );
    vertices[1] = Vertex(
        Vector(width / 2 / FLAT_ELEMENT_COEFF, height / 2 / FLAT_ELEMENT_COEFF, FLAT_ELEMENT_FAR_VALUE), // position
        Vector(1.0f, 0.0f, 0.0f), // normal
        material.DiffuseColor, // color
        TextureCoord(1.0f, 0.0f) // texture coords
    );
    vertices[2] = Vertex(
        Vector(width / 2 / FLAT_ELEMENT_COEFF * -1, height / 2 / FLAT_ELEMENT_COEFF * -1, FLAT_ELEMENT_FAR_VALUE), // position
        Vector(0.0f, 1.0f, 1.0f), // normal
        material.DiffuseColor, // color
        TextureCoord(0.0f, 1.0f) // texture coords
    );
    vertices[3] = Vertex(
        Vector(width / 2 / FLAT_ELEMENT_COEFF, height / 2 / FLAT_ELEMENT_COEFF * -1, FLAT_ELEMENT_FAR_VALUE), // position
        Vector(0.0f, 1.0f, 1.0f), // normal
        material.DiffuseColor, // color
        TextureCoord(1.0f, 1.0f) // texture coords
    );
}

void FlatElement::setPosition(float x, float y)
{
    absoluteTransformation.setTranslation(Vector(x / FLAT_ELEMENT_COEFF, y / FLAT_ELEMENT_COEFF, 0.0f));
}

void FlatElement::setRotation(float degrees)
{
    degrees *= -1;
    absoluteTransformation.setRotationDegrees(Vector(0.0f, 0.0f, degrees));
}

void FlatElement::setSize(float width, float height)
{
    size.Width = width;
    size.Height = height;
}

void FlatElement::setOpacity(float opacity)
{
    material.DiffuseColor.setAlpha((unsigned int) (opacity * 255.0f));
}

void FlatElement::setTexture(const std::string& filePath)
{
    texture = scene->getGame()->getAssetManager()->loadTexture(filePath);
}

void FlatElement::setDiffuseColor(irr::video::SColor color)
{
    material.DiffuseColor = color;
}

void FlatElement::show()
{
    visible = true;
}

void FlatElement::hide()
{
    visible = false;
}

void FlatElement::flip(FlipType type)
{
    if (type == FLIP_HORIZONTALLY) {
        irr::core::vector2df temp = vertices[0].TCoords;
        vertices[0].TCoords = vertices[1].TCoords;
        vertices[1].TCoords = temp;
        temp = vertices[2].TCoords;
        vertices[2].TCoords = vertices[3].TCoords;
        vertices[3].TCoords = temp;
    }
    if (type == FLIP_VERTICALLY) {
        irr::core::vector2df temp = vertices[0].TCoords;
        vertices[0].TCoords = vertices[2].TCoords;
        vertices[2].TCoords = temp;
        temp = vertices[1].TCoords;
        vertices[1].TCoords = vertices[3].TCoords;
        vertices[3].TCoords = temp;
    }
}

Vector FlatElement::getPosition()
{
    irr::core::vector3df translation = absoluteTransformation.getTranslation();

    return Vector(
        translation.X * FLAT_ELEMENT_COEFF,
        translation.Y * FLAT_ELEMENT_COEFF,
        translation.Z * FLAT_ELEMENT_COEFF
    );
}

}}}
