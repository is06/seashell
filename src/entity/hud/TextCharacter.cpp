#include "../../../include/seashell/entity/hud/TextCharacter.h"
#include "../../../include/seashell/Scene.h"
#include "../../../include/seashell/Game.h"

namespace seashell { namespace entity { namespace hud {

TextCharacter::TextCharacter(
    Scene* scene,
    int code,
    float x,
    float y,
    irr::video::SColor color
) : FlatElement(scene, x, y, 28, 28, true)
{
    texture = scene->getGame()->getAssetManager()->loadTexture("hud/fonts/chiaro_std.png");
    if (texture) {
        material.setTexture(0, texture);
    }

    hide();
    setPosition(x, y);

    float space = 1.0f / 16.0f;

    float endU = ((code / 16) + 1) * space;
    float endV = ((code - ((code / 16) * 16)) + 1) * space;
    float startU = endU - space;
    float startV = endV - space;

    vertices[0].TCoords.X = startV;
    vertices[0].TCoords.Y = startU;
    vertices[1].TCoords.X = endV;
    vertices[1].TCoords.Y = startU;
    vertices[2].TCoords.X = startV;
    vertices[2].TCoords.Y = endU;
    vertices[3].TCoords.X = endV;
    vertices[3].TCoords.Y = endU;

    material.DiffuseColor = color;
}

void TextCharacter::render()
{
    FlatElement::render();
}

TextCharacter::~TextCharacter()
{

}

}}}
