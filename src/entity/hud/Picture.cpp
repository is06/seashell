#include "../../../include/seashell/entity/hud/Picture.h"
#include "../../../include/seashell/Scene.h"
#include "../../../include/seashell/Game.h"

namespace seashell { namespace entity { namespace hud {

//! Constructor
/**
 * \param seashell::Scene* scene where the picture is
 * \param float x X position on the screen
 * \param float y Y position on the screen
 * \param float width Width of the picture
 * \param float height Height of the picture
 * \param std::string& filePath The path of the picture on disk
 * \param bool useAlphaBlending If the Alpha Blending mode is active
 */
Picture::Picture(
    Scene* scene,
    float x,
    float y,
    float width,
    float height,
    const std::string& filePath,
    bool useAlphaBlending
) : FlatElement(scene, x, y, width, height, useAlphaBlending)
{
    texture = scene->getGame()->getAssetManager()->loadTexture(filePath);
    if (texture) {
        material.setTexture(0, texture);
    }
}

//! Called on every frame render the picture on the screen
void Picture::render()
{
    FlatElement::render();
}

//! Destructor
Picture::~Picture()
{

}

}}}
