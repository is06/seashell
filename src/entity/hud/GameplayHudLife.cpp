#include "../../../include/seashell/Game.h"
#include "../../../include/seashell/entity/hud/GameplayHudLife.h"
#include "../../../include/seashell/entity/hud/Picture.h"

namespace seashell { namespace entity { namespace hud {

GameplayHudLife::GameplayHudLife(Scene* scene) : HudEntity(scene)
{
    position = irr::core::position2di(-272, 200);

    life = 13;
    maxLife = 12;
    float positionXOffset = 12;
    float positionYOffset = 12;

    for (char i = 0; i < maxLife / 4; i++) {
        Picture* heart = new Picture(
            scene,
            position.X + positionXOffset,
            position.Y - positionYOffset,
            24, 24,
            "hud/gameplay/heart.png"
        );
        heart->setDiffuseColor(irr::video::SColor(255, 255, 70, 50));
        hearts.push_back(heart);
        positionXOffset += 22;
        if (i == 9) {
            positionXOffset = 12;
            positionYOffset = 34;
        }
    }
}

void GameplayHudLife::render()
{
    HudEntity::render();

    for (Picture* heart : hearts) {
        if (heart) {
            heart->render();
        }
    }
}

void GameplayHudLife::addLife(unsigned char value)
{
    life += value;
    if (life > 80) {
        life = 80;
    }
}

void GameplayHudLife::removeLife(unsigned char value)
{
    life -= value;
}

GameplayHudLife::~GameplayHudLife()
{
    for (Picture* heart : hearts) {
        delete heart;
    }
}

}}}
