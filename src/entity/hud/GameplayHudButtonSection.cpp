#include "../../../include/seashell/entity/hud/GameplayHudButtonSection.h"
#include "../../../include/seashell/entity/hud/GameplayHudButton.h"

namespace seashell { namespace entity { namespace hud {

GameplayHudButtonSection::GameplayHudButtonSection(Scene* scene) : HudEntity(scene)
{
    item1Button = new GameplayHudButton(scene, 128, 176, irr::video::SColor(255, 192, 192, 192));
    item1Button->disable();

    swordButton = new GameplayHudButton(scene, 188, 176, irr::video::SColor(255, 0, 160, 0));
    swordButton->setItem("sword");

    item2Button = new GameplayHudButton(scene, 248, 176, irr::video::SColor(255, 192, 192, 192));
    item2Button->disable();

    actionButton = new GameplayHudButton(scene, 218, 124, irr::video::SColor(255, 96, 80, 240));
    actionButton->setText("attack");
}

void GameplayHudButtonSection::render()
{
    HudEntity::render();

    swordButton->render();
    actionButton->render();
    item1Button->render();
    item2Button->render();
}

GameplayHudButtonSection::~GameplayHudButtonSection()
{
    delete swordButton;
    delete actionButton;
    delete item1Button;
    delete item2Button;
}

}}}
