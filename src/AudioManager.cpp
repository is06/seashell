#include "../include/seashell/AudioManager.h"
#include "sndfile.h"
#include <iostream>
#include <vector>

namespace seashell {
    
AudioManager::AudioManager()
{
    device = alcOpenDevice(NULL);
    if (!device) {
        throw std::runtime_error("AL device creation error");
    }

    context = alcCreateContext(device, NULL);
    if (!context) {
        throw std::runtime_error("AL context creation error");
    }

    if (!alcMakeContextCurrent(context)) {
        throw std::runtime_error("AL context activation error");
    }
}

void AudioManager::update()
{
    ALint status;
    alGetSourcei(mainSource, AL_SOURCE_STATE, &status);
    if (status == AL_STOPPED) {
        alDeleteBuffers(1, &mainBuffers.at(0));
    }
}

void AudioManager::playSound(const std::string& name)
{
    alGenSources(1, &mainSource);

    ALuint buffer = getBufferFromFile("assets/audio/sounds/" + name + ".wav");
    if (buffer) {
        mainBuffers.push_back(buffer);

        alSourcei(mainSource, AL_BUFFER, mainBuffers[0]);
        alSourcePlay(mainSource);
    }
}

ALuint AudioManager::getBufferFromFile(const std::string& filePath)
{
    SF_INFO fileInfo;
    SNDFILE* file = sf_open(filePath.c_str(), SFM_READ, &fileInfo);
    if (!file) {
        return 0;
    }

    ALsizei sampleCount = static_cast<ALsizei>(fileInfo.channels * fileInfo.frames);
    ALsizei sampleRate = static_cast<ALsizei>(fileInfo.samplerate);

    std::vector<ALshort> samples(sampleCount);
    if (sf_read_short(file, &samples[0], sampleCount) < sampleCount) {
        return 0;
    }

    sf_close(file);

    ALenum format;
    switch (fileInfo.channels) {
        case 1: format = AL_FORMAT_MONO16; break;
        case 2: format = AL_FORMAT_STEREO16; break;
        default: return 0;
    }

    ALuint buffer;
    alGenBuffers(1, &buffer);
    alBufferData(buffer, format, &samples[0], sampleCount * sizeof(ALushort), sampleRate);

    if (alGetError() != AL_NO_ERROR) {
        return 0;
    }

    return buffer;
}

AudioManager::~AudioManager()
{
    alSourcei(mainSource, AL_BUFFER, 0);
    alDeleteSources(1, &mainSource);

    alcMakeContextCurrent(NULL);
    alcDestroyContext(context);
    alcCloseDevice(device);
}

}