#include "../include/seashell/MapData.h"

namespace pt = boost::property_tree;

namespace seashell {

void MapData::load(const std::string& filePath)
{
    pt::ptree tree;
    pt::json_parser::read_json(filePath, tree);

    name = tree.get("name", "none");
    title = tree.get("title", "Untitled");
    type = tree.get("type", "outside");
    music = tree.get("music", "none");

    for (const pt::ptree::value_type& v : tree.get_child("sections")) {
        addSection(v);
    }
    for (const pt::ptree::value_type& v : tree.get_child("entities")) {
        addEntity(v);
    }
}

void MapData::addSection(const pt::ptree::value_type& v)
{
    pt::ptree sectionTree = v.second;

    MapSectionData section = MapSectionData();
    std::string name = sectionTree.get<std::string>("name");

    for (const pt::ptree::value_type& v : sectionTree.get_child("parts")) {
        section.addPart(v);
    }

    sections[name] = section;
}

void MapData::addEntity(const pt::ptree::value_type& v)
{
    pt::ptree entityTree = v.second;

    MapEntityData entity = MapEntityData();
    entity.type = entityTree.get<std::string>("type");
    entities[entityTree.get<std::string>("name")] = entity;
}

void MapSectionData::addPart(const pt::ptree::value_type& v)
{
    pt::ptree partTree = v.second;

    MapPartData part = MapPartData();
    std::string name = partTree.get<std::string>("name");
    part.typeIdentifier = partTree.get<std::string>("type");
    part.solid = partTree.get<bool>("solid");

    if (part.typeIdentifier == "polygon") {
        for (const pt::ptree::value_type& v : partTree.get_child("vertices")) {
            part.addVertex(v);
        }
        for (const pt::ptree::value_type& v : partTree.get_child("indices")) {
            part.addIndice(v);
        }
    } else if (part.typeIdentifier == "object") {
        part.filePath = partTree.get<std::string>("file");
        part.scale.X = partTree.get_child("scale").get<float>("x");
        part.scale.Y = partTree.get_child("scale").get<float>("y");
        part.scale.Z = partTree.get_child("scale").get<float>("z");
        part.position.X = partTree.get_child("position").get<float>("x");
        part.position.Y = partTree.get_child("position").get<float>("y");
        part.position.Z = partTree.get_child("position").get<float>("z");
        part.rotation.X = partTree.get_child("rotation").get<float>("x");
        part.rotation.Y = partTree.get_child("rotation").get<float>("y");
        part.rotation.Z = partTree.get_child("rotation").get<float>("z");
    }

    parts[name] = part;
}

void MapPartData::addVertex(const pt::ptree::value_type& v)
{
    pt::ptree vertexTree;

    irr::video::S3DVertex vertex = irr::video::S3DVertex();

    vertex.Pos.X = v.second.get<float>("x");
    vertex.Pos.Y = v.second.get<float>("y");
    vertex.Pos.Z = v.second.get<float>("z");
    vertex.TCoords.X = v.second.get<float>("u");
    vertex.TCoords.Y = v.second.get<float>("v");
    vertex.Color.setRed(v.second.get<unsigned int>("c.r"));
    vertex.Color.setGreen(v.second.get<unsigned int>("c.g"));
    vertex.Color.setBlue(v.second.get<unsigned int>("c.b"));
    vertex.Color.setAlpha(v.second.get<unsigned int>("c.a"));

    vertices.push_back(vertex);
}

void MapPartData::addIndice(const pt::ptree::value_type& v)
{
    // todo: fix this!!
    //indices.push_back(v.second.data());
}

}
