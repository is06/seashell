#include "../include/seashell/KeyboardManager.h"

namespace seashell {

KeyboardManager::KeyboardManager()
{
    for (unsigned int i = 0; i < irr::KEY_KEY_CODES_COUNT; i++) {
        keyDown[i] = false;
        keyOnce[i] = false;
    }

    anyDown = false;
    anyOnce = false;
}

bool KeyboardManager::isKeyDown(irr::EKEY_CODE key)
{
    return keyDown[key];
}

bool KeyboardManager::isKeyDownOnce(irr::EKEY_CODE key)
{
    if (!keyOnce[key]) {
        if (isKeyDown(key)) {
            keyOnce[key] = true;
            return true;
        }
    } else {
        if (!isKeyDown(key)) {
            keyOnce[key] = false;
        }
    }
    return false;
}

bool KeyboardManager::trigger(const irr::SEvent& event)
{
    if (event.EventType == irr::EET_KEY_INPUT_EVENT) {
        keyDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
    }

    return false;
}

KeyboardManager::~KeyboardManager()
{

}

}