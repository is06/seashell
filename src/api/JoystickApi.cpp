#include "../../include/seashell/api/JoystickApi.h"
#include "../../include/seashell/Game.h"

namespace seashell { namespace api {
        
JoystickApi::JoystickApi(Scene* scene) : AbstractApi(scene)
{
    type = JOYSTICK_LEFT;
}

void JoystickApi::update()
{
    
}

float JoystickApi::angle()
{
    if (type == JOYSTICK_RIGHT) {
        return scene->getGame()->getEventManager()->getGamepadManager()->getRightJoystickAngle();
    }
    return scene->getGame()->getEventManager()->getGamepadManager()->getLeftJoystickAngle();
}

float JoystickApi::force()
{
    if (type == JOYSTICK_RIGHT) {
        return scene->getGame()->getEventManager()->getGamepadManager()->getRightJoystickForce();
    }
    return scene->getGame()->getEventManager()->getGamepadManager()->getLeftJoystickForce();
}

JoystickDirection JoystickApi::direction()
{
    return JOYSTICK_DIRECTION_NONE;
}

bool JoystickApi::isClicked()
{
    return false;
}

void JoystickApi::setType(JoystickType type)
{
    this->type = type;
}

JoystickApi::~JoystickApi()
{

}

}}
