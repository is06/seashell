#include "../../include/seashell/api/SoundApi.h"
#include "../../include/seashell/Game.h"

#include <string>

namespace seashell { namespace api {
        
SoundApi::SoundApi(Scene* scene) : AbstractApi(scene)
{

}

void SoundApi::update()
{
    
}

void SoundApi::play(const std::string& name)
{
    scene->getGame()->getAudioManager()->playSound(name);
}

SoundApi::~SoundApi()
{

}

}}
