#include "../../include/seashell/api/ControlApi.h"
#include "../../include/seashell/api/ControlButtonApi.h"
#include "../../include/seashell/api/JoystickApi.h"
#include "../../include/seashell/Scene.h"

namespace seashell { namespace api {
        
ControlApi::ControlApi(Scene* scene) : AbstractApi(scene)
{
    buttonApi = new ControlButtonApi(scene);
    joystickApi = new JoystickApi(scene);
}

void ControlApi::update()
{
    
}

ControlButtonApi* ControlApi::button(ControlButton which)
{
    buttonApi->setButton(which);
    return buttonApi;
}

JoystickApi* ControlApi::joystick(JoystickType which)
{
    joystickApi->setType(which);
    return joystickApi;
}

ControlApi::~ControlApi()
{
    delete buttonApi;
    delete joystickApi;
}

}}
