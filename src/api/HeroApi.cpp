#include "../../include/seashell/api/HeroApi.h"
#include "../../include/seashell/api/CollisionApi.h"
#include "../../include/seashell/entity/world/Hero.h"
#include "../../include/seashell/Scene.h"

namespace seashell { namespace api {
        
HeroApi::HeroApi(Scene* scene) : AbstractApi(scene)
{
    hero = new entity::world::Hero(scene, scene->getMainCamera());
    collisions = new CollisionApi(scene);
}

void HeroApi::update()
{
    hero->update();
}

CollisionApi* HeroApi::collides(EventType when)
{
    return collisions;
}

HeroApi::~HeroApi()
{
    delete hero;
    delete collisions;
}

}}
