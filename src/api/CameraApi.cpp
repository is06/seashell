#include "../../include/seashell/api/CameraApi.h"
#include "../../include/seashell/Scene.h"
#include "../../include/seashell/entity/world/ThirdPersonCamera.h"
#include "../../include/seashell/entity/world/FreeCamera.h"
#include "../../include/seashell/entity/world/Character.h"

#include <iostream>

using namespace std;

namespace seashell { namespace api {

CameraApi::CameraApi(Scene* scene) : AbstractApi(scene)
{
    switch (scene->getSceneType()) {
        case scene::TYPE_MAP:
            cameras["main"] = new entity::world::ThirdPersonCamera(scene);
            break;
        case scene::TYPE_SIMPLE:
            cameras["main"] = new entity::world::FreeCamera(scene);
            break;
        default:
            break;
    }
}

void CameraApi::update()
{
    
}

entity::world::Camera* CameraApi::get(const std::string& name)
{
    return cameras[name];
}

CameraApi::~CameraApi()
{
    for (const auto& camera : cameras) {
        delete camera.second;
    }
}

}}
