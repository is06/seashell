#include "../../include/seashell/api/EntityApi.h"

namespace seashell { namespace api {
        
EntityApi::EntityApi(Scene* scene) : AbstractApi(scene)
{

}

void EntityApi::update()
{
    
}

void EntityApi::addBoxSensor()
{
    
}

void EntityApi::addChest(const std::string& name, float x, float y, float z, float direction, ItemIdentifier content)
{
    
}

void EntityApi::addDoor()
{
    
}

void EntityApi::addEnemy()
{
    
}

void EntityApi::addItem()
{
    
}

void EntityApi::addNpc()
{
    
}

void EntityApi::addPlaneSensor()
{
    
}

void EntityApi::addStartingPoint()
{
    
}

void EntityApi::addTransportable()
{
    
}

void EntityApi::addWarper()
{
    
}

EntityApi::~EntityApi()
{

}

}}
