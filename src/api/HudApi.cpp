#include "../../include/seashell/api/HudApi.h"
#include "../../include/seashell/entity/hud/GameplayHudButtonSection.h"
#include "../../include/seashell/entity/hud/GameplayHudLife.h"
#include "../../include/seashell/entity/hud/GameplayHudMinimap.h"
#include "../../include/seashell/entity/hud/GameplayHudRupees.h"

namespace seashell { namespace api {

HudApi::HudApi(Scene* scene) : AbstractApi(scene)
{
    hudButtons = new entity::hud::GameplayHudButtonSection(scene);
    life = new entity::hud::GameplayHudLife(scene);
    minimap = new entity::hud::GameplayHudMinimap(scene);
    rupees = new entity::hud::GameplayHudRupees(scene);
}

void HudApi::update()
{
    hudButtons->render();
    life->render();
    minimap->render();
    rupees->render();
}

HudApi::~HudApi()
{
    delete hudButtons;
    delete life;
    delete minimap;
    delete rupees;
}

}}
