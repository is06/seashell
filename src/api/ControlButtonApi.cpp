#include "../../include/seashell/api/ControlButtonApi.h"
#include "../../include/seashell/Game.h"

namespace seashell { namespace api {
        
irr::EKEY_CODE ControlButtonApi::keyboardMapping[BUTTON_COUNT];

ControlButtonApi::ControlButtonApi(Scene* scene) : AbstractApi(scene)
{
    keyboardMapping[BUTTON_START] = irr::KEY_RETURN;
    keyboardMapping[BUTTON_ACTION] = irr::KEY_SPACE;
    keyboardMapping[BUTTON_SWORD] = irr::KEY_KEY_C;
    keyboardMapping[BUTTON_ITEM_1] = irr::KEY_KEY_X;
    keyboardMapping[BUTTON_ITEM_2] = irr::KEY_KEY_V;
    keyboardMapping[BUTTON_Z_TRIGGER] = irr::KEY_KEY_D;
    keyboardMapping[BUTTON_L_TRIGGER] = irr::KEY_KEY_1;
    keyboardMapping[BUTTON_R_TRIGGER] = irr::KEY_KEY_2;
}

void ControlButtonApi::update()
{
    
}

bool ControlButtonApi::isPressed()
{
    return scene->getGame()->getEventManager()->getKeyboardManager()->isKeyDownOnce(keyboardMapping[button]);
}

bool ControlButtonApi::isMaintained()
{
    return scene->getGame()->getEventManager()->getKeyboardManager()->isKeyDown(keyboardMapping[button]);
}

void ControlButtonApi::setButton(ControlButton button)
{
    this->button = button;
}

ControlButtonApi::~ControlButtonApi()
{

}

}}
