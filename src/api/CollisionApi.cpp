#include "../../include/seashell/api/CollisionApi.h"

#include <string>

namespace seashell { namespace api {
        
CollisionApi::CollisionApi(Scene* scene) : AbstractApi(scene)
{

}

void CollisionApi::update()
{
    
}

bool CollisionApi::withBoxSensor(const std::string& name)
{
    return false;
}

bool CollisionApi::withChest(const std::string& name)
{
    return false;
}

bool CollisionApi::withDoor(const std::string& name)
{
    return false;
}

bool CollisionApi::withEnemy(const std::string& name)
{
    return false;
}

bool CollisionApi::withItem(const std::string& name)
{
    return false;
}

bool CollisionApi::withNpc(const std::string& name)
{
    return false;
}

bool CollisionApi::withPlaneSensor(const std::string& name)
{
    return false;
}

bool CollisionApi::withTransportable(const std::string& name)
{
    return false;
}

bool CollisionApi::withWarper(const std::string& name)
{
    return false;
}

CollisionApi::~CollisionApi()
{

}

}}
