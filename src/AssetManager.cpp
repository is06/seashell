#include "../include/seashell/AssetManager.h"
#include "../include/seashell/Game.h"

#include <irrlicht/irrlicht.h>

namespace seashell {
    
//! Constructor
AssetManager::AssetManager(Game* game)
{
    this->game = game;
}

//! Return a IMesh object from a mesh file
/**
 * \param std::string& path to the mesh on disk
 * \return irr::scene::IMesh*
 */
irr::scene::IMesh* AssetManager::loadMesh(const std::string& path)
{
    return game->getSceneManager()->getMesh(path.c_str());
}

//! Return a ITexture object from an image file
/**
 * \param std::string& path to the texture on disk
 * \return irr::video::ITexture*
 */
irr::video::ITexture* AssetManager::loadTexture(const std::string& path)
{
    return game->getVideoDriver()->getTexture(("assets/textures/" + path).c_str());
}

//! Destructor
AssetManager::~AssetManager()
{
    
}

}
