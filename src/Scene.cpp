#include "../include/seashell/Scene.h"
#include "../include/seashell/Game.h"
#include "../include/seashell/MapData.h"
#include "../include/seashell/entity/Timer.h"
#include "../include/seashell/entity/hud/Picture.h"
#include "../include/seashell/entity/hud/DialogBox.h"
#include "../include/seashell/entity/world/MapPart.h"
#include "../include/seashell/api/MapApi.h"
#include "../include/seashell/api/ControlApi.h"
#include "../include/seashell/api/EntityApi.h"
#include "../include/seashell/api/SoundApi.h"
#include "../include/seashell/api/CameraApi.h"

#include <iostream>

using namespace std;

namespace seashell {

//! Constructor
/**
 * \param seashell::Game* game the current game object
 * \param const std::string& name of the scene, used to load scene json data
 */
Scene::Scene(Game* game, const std::string& name)
{
    this->game = game;
    this->name = name;

    map = new api::MapApi(this);
    control = new api::ControlApi(this);
    entity = new api::EntityApi(this);
    sound = new api::SoundApi(this);
    camera = new api::CameraApi(this);

    currentTime = 0.0f;

    tempDialog = new entity::hud::DialogBox(
        this,
        entity::hud::DIALOG_BOX_TYPE_NORMAL,
        entity::hud::DIALOG_BOX_POSITION_BOTTOM,
        "With the blade Timeless, he will\\ncommand the power of The Gods\\nwithout the wisdom of why it\\ncommands as it does"
    );

    loadFromData();
}

//! Called on every frame to execute events of the scene
void Scene::events()
{
    map->update();
    control->update();
    entity->update();
    camera->update();
    sound->update();
}

//! Called on every frame to render all hud entities
void Scene::hudRender()
{
    for (auto const &entity : hudEntities) {
        entity.second->render();
    }

    tempDialog->render();
}

scene::SceneType Scene::getSceneType()
{
    return type;
}

//! Starts the fade in transition
/**
 * \param float speed of the transition
 * \param seashell::FadeColor can be FADE_COLOR_WHITE or FADE_COLOR_BLACK
 */
void Scene::fadeIn(float speed, FadeColor color)
{
    throw std::runtime_error("Scene::fadeIn not implemented yet!");
}

//! Starts the fade out transition
/**
 * \param float speed of the transition
 * \param seashell::FadeColor can be FADE_COLOR_WHITE or FADE_COLOR_BLACK
 */
void Scene::fadeOut(float speed, FadeColor color)
{
    throw std::runtime_error("Scene::fadeOut not implemented yet!");
}

Game* Scene::getGame()
{
    return game;
}

entity::world::Camera* Scene::getMainCamera()
{
    return camera->get("main");
}

void Scene::loadFromData()
{
    try {
        MapData data = MapData();
        data.load("data/map/" + name + ".json");
        for (const auto& section : data.sections) {
            loadSection(section.second);
        }
    } catch (std::exception &e) {
        throw std::runtime_error(e.what());
    }
}

void Scene::loadSection(const MapSectionData& section)
{
    for (const auto& data : section.parts) {
        mapParts[data.first] = new entity::world::MapPart(this);
        if (data.second.typeIdentifier == "polygon") {
            mapParts[data.first]->loadMeshFromVertices(data.second.vertices, data.second.indices);
        } else if (data.second.typeIdentifier == "object") {
            mapParts[data.first]->loadMeshFromFile("assets/meshes/maps/" + data.second.filePath);
            mapParts[data.first]->createNode(
                data.second.position,
                data.second.rotation,
                data.second.scale
            );
        }
    }
}

//! Destructor
Scene::~Scene()
{
    for (auto const &entity : hudEntities) {
        delete entity.second;
    }

    delete map;
    delete control;
    delete entity;
    delete camera;
    delete sound;

    delete tempDialog;
}

}
