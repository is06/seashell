#include "../../include/seashell/scene/Map.h"
#include "../../include/seashell/api/HeroApi.h"
#include "../../include/seashell/api/HudApi.h"
#include "../../include/seashell/api/ControlApi.h"
#include "../../include/seashell/api/ControlButtonApi.h"
#include "../../include/seashell/api/SoundApi.h"

#include <iostream>

using namespace std;

namespace seashell { namespace scene {

//! Constructor
/**
 * \param seashell::Game* game the current game object
 * \param const std::string& name of the scene
 */
Map::Map(seashell::Game* game, const std::string& name) : Scene(game, name)
{
    hero = new api::HeroApi(this);
    hud = new api::HudApi(this);
}

//! Called on every frame to execute events of the playable scene
void Map::events()
{
    Scene::events();

    if (control->button(BUTTON_ACTION)->isPressed()) {
        cout << "action is pressed" << endl;
        sound->play("dialogue_done");
    }

    hero->update();
    hud->update();
}

//! Destructor
Map::~Map()
{
    delete hero;
    delete hud;
}

}}
