#include "../../include/seashell/scene/SimpleScene.h"

namespace seashell { namespace scene {

//! Constructor
/**
 * \param seashell::Game* game the current game object
 */
SimpleScene::SimpleScene(Game* game, const std::string& name) : Scene(game, name)
{
    
}

//! Called on every frame to execute events of the scene
void SimpleScene::events()
{
    Scene::events();
}

//! Destructor
SimpleScene::~SimpleScene()
{

}

}}
