#include "../../include/seashell/scene/Outside.h"

namespace seashell { namespace scene {

//! Constructor
/**
 * \param seashell::Game* game the current game object
 */
Outside::Outside(Game* game, const std::string& name) : Map(game, name)
{
    
}

//! Called on every frame to execute events of the outside map
void Outside::events()
{
    Map::events();
}

//! Destructor
Outside::~Outside()
{

}

}}
