#include "../../include/seashell/scene/Dungeon.h"

namespace seashell { namespace scene {
    
//! Constructor
/**
 * \param seashell::Game* game the current game object
 */
Dungeon::Dungeon(Game* game, const std::string& name) : Map(game, name)
{
    
}

//! Called on every frame to execute events of the dungeon map
void Dungeon::events()
{
    Map::events();
}

//! Destructor
Dungeon::~Dungeon()
{

}

}}
