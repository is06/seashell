#include "../include/seashell/Entity.h"

namespace seashell {
    
//! Constructor
Entity::Entity(Scene* scene)
{
    this->scene = scene;
}

//! Called on every frame to do some updates to the entity
void Entity::update()
{

}

//! Destructor
Entity::~Entity()
{

}

}
