#include "../include/seashell/MapSectionManager.h"
#include "../include/seashell/entity/world/MapPart.h"

namespace seashell {

MapSectionManager::MapSectionManager(Scene* scene)
{
    this->scene = scene;

    parts[0] = NULL;
    parts[1] = NULL;
}

void MapSectionManager::loadSection(const std::string& mapName, const std::string& sectionName, const irr::core::vector3df& position)
{
    if (parts[0]) {
        swapLevelMeshes();
    }

    irr::core::vector3df defaultVector;

    parts[0] = new entity::world::MapPart(scene);
    parts[0]->loadMeshFromFile("assets/meshes/map/" + mapName + "/" + sectionName + ".obj");
    parts[0]->createNode(position, defaultVector, defaultVector);

    unloadUnusedParts();
}

void MapSectionManager::swapLevelMeshes()
{
    entity::world::MapPart* tmp;
    tmp = parts[1];
    parts[1] = parts[0];
    parts[0] = tmp;
}

void MapSectionManager::unloadUnusedParts()
{
    if (parts[1]) {
        delete parts[1];
    }
}

MapSectionManager::~MapSectionManager()
{
    if (parts[0]) {
        delete parts[0];
    }
    if (parts[1]) {
        delete parts[1];
    }
}

}
