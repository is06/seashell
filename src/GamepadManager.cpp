#include "../include/seashell/GamepadManager.h"

namespace seashell {

GamepadManager::GamepadManager()
{

}

float GamepadManager::getLeftJoystickForce()
{
    return leftJoystickForce;
}

float GamepadManager::getLeftJoystickAngle()
{
    return leftJoystickAngle;
}

float GamepadManager::getRightJoystickForce()
{
    return rightJoystickForce;
}

float GamepadManager::getRightJoystickAngle()
{
    return rightJoystickAngle;
}

unsigned int GamepadManager::getPressedButtons()
{
    return buttonStates;
}

bool GamepadManager::trigger(const irr::SEvent& event)
{
    setButtonStates(event);
    setJoystickStates(event);

    return false;
}

void GamepadManager::setButtonStates(const irr::SEvent& event)
{
    buttonStates = event.JoystickEvent.ButtonStates;
}

void GamepadManager::setJoystickStates(const irr::SEvent& event)
{
    float lXAxis = (event.JoystickEvent.Axis[irr::SEvent::SJoystickEvent::AXIS_X] + 100) / 32768.0f;
    float lYAxis = ((event.JoystickEvent.Axis[irr::SEvent::SJoystickEvent::AXIS_Y] + 100) / 32768.0f) * -1;
    float rXAxis = (event.JoystickEvent.Axis[irr::SEvent::SJoystickEvent::AXIS_U] + 100) / 32768.0f;
    float rYAxis = ((event.JoystickEvent.Axis[irr::SEvent::SJoystickEvent::AXIS_R] + 100) / 32768.0f) * -1;

    if (lXAxis != 0) {
        if (lXAxis < 0) {
            leftJoystickAngle = irr::core::radToDeg(irr::core::PI + atan(lYAxis/lXAxis));
        } else {
            if (lYAxis < 0) {
                leftJoystickAngle = irr::core::radToDeg(2 * irr::core::PI + atan(lYAxis/lXAxis));
            } else {
                leftJoystickAngle = irr::core::radToDeg(atan(lYAxis/lXAxis));
            }
        }
    } else {
        leftJoystickAngle = 0;
    }
    if (rXAxis != 0) {
        if (rXAxis < 0) {
            rightJoystickAngle = irr::core::radToDeg(irr::core::PI + atan(rYAxis/rXAxis));
        } else {
            if (rYAxis < 0) {
                rightJoystickAngle = irr::core::radToDeg(2 * irr::core::PI + atan(rYAxis/rXAxis));
            } else {
                rightJoystickAngle = irr::core::radToDeg(atan(rYAxis/rXAxis));
            }
        }
    } else {
        rightJoystickAngle = 0;
    }

    if (fabs(lXAxis) > 0.05f) {
        if (lXAxis > 0) {
            lXAxis = (lXAxis * 255);
        } else {
            lXAxis = (fabs(lXAxis) * 255);
        }
    } else {
        lXAxis = 0;
    }
    if (fabs(lYAxis) > 0.05f) {
        if (lYAxis > 0) {
            lYAxis = (lYAxis * 255);
        } else {
            lYAxis = (fabs(lYAxis) * 255);
        }
    } else {
        lYAxis = 0;
    }
    if (fabs(rXAxis) > 0.05f) {
        if (rXAxis > 0) {
            rXAxis = (rXAxis * 255);
        } else {
            rXAxis = (fabs(rXAxis) * 255);
        }
    } else {
        rXAxis = 0;
    }
    if (fabs(rYAxis) > 0.05f) {
        if (rYAxis > 0) {
            rYAxis = (rYAxis * 255);
        } else {
            rYAxis = (fabs(rYAxis) * 255);
        }
    } else {
        rYAxis = 0;
    }
    
    if (lXAxis > 255) lXAxis = 255;
    if (lYAxis > 255) lYAxis = 255;
    if (rXAxis > 255) rXAxis = 255;
    if (rYAxis > 255) rYAxis = 255;

    leftJoystickForce = irr::core::max_(irr::core::round_(lXAxis), irr::core::round_(lYAxis));
    rightJoystickForce = irr::core::max_(irr::core::round_(rXAxis), irr::core::round_(rYAxis));
}

GamepadManager::~GamepadManager()
{

}

}