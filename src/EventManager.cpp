#include "../include/seashell/EventManager.h"

namespace seashell {
    
EventManager::EventManager() : irr::IEventReceiver()
{
    gamepad = new GamepadManager();
    keyboard = new KeyboardManager();
}

bool EventManager::OnEvent(const irr::SEvent& event)
{
    switch (event.EventType) {
        case irr::EET_KEY_INPUT_EVENT:
            return keyboard->trigger(event);
        case irr::EET_JOYSTICK_INPUT_EVENT:
            return gamepad->trigger(event);
        case irr::EET_MOUSE_INPUT_EVENT:
            return false;
        default:
            return false;
    }
}

GamepadManager* EventManager::getGamepadManager()
{
    return gamepad;
}

KeyboardManager* EventManager::getKeyboardManager()
{
    return keyboard;
}

EventManager::~EventManager()
{
    delete gamepad;
    delete keyboard;
}

}
