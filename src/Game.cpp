#include "../include/seashell/Game.h"

namespace seashell {

//! Constructor
Game::Game()
{
    device = nullptr;
    videoDriver = nullptr;
    sceneManager = nullptr;
    currentScene = nullptr;

    assetManager = new AssetManager(this);
    audioManager = new AudioManager();
    eventManager = new EventManager();

    speedFactor = 0.0f;

    initIrrlicht();
}

//! Main loop of the game
void Game::run()
{
    if (currentScene == nullptr) {
        throw std::runtime_error("No current scene defined");
    }

    unsigned int lastCycleTime, loopTime = 0;

    while (device->run()) {
        lastCycleTime = device->getTimer()->getRealTime() - loopTime;
        loopTime = device->getTimer()->getRealTime();
        speedFactor = lastCycleTime / 1000.0f;
        if (speedFactor > 1.0f) {
            speedFactor = 1.0f;
        }
        if (speedFactor < 0.0f) {
            speedFactor = 0.0f;
        }

        videoDriver->beginScene(true, true, irr::video::SColor(255, 128, 128, 128));
        currentScene->events();
        sceneManager->drawAll();
        currentScene->hudRender();
        audioManager->update();
        videoDriver->endScene();

        device->yield();
    }
}

void Game::setTitle(const std::wstring& title)
{
    device->setWindowCaption(title.c_str());
}

void Game::startScene(Scene* scene)
{
    currentScene = scene;
}

//! Init the Irrlicht objects
void Game::initIrrlicht()
{
    irr::SIrrlichtCreationParameters deviceParameters;
    deviceParameters.Bits = 32;
    deviceParameters.DriverType = irr::video::EDT_OPENGL;
    deviceParameters.EventReceiver = eventManager;
    deviceParameters.WindowSize = irr::core::dimension2du(640, 480);

    device = createDeviceEx(deviceParameters);
    if (device == nullptr) {
        throw std::runtime_error("Unable to create the irrlicht device");
    }

    videoDriver = device->getVideoDriver();
    sceneManager = device->getSceneManager();
}

//! Return the current Irrlicht video driver
/**
 * \param irr::video::IVideoDriver*
 */
irr::video::IVideoDriver* Game::getVideoDriver()
{
    return videoDriver;
}

//! Return the current Irrlicht scene manager
/**
 * \return irr::scene::ISceneManager*
 */
irr::scene::ISceneManager* Game::getSceneManager()
{
    return sceneManager;
}

//! Return the asset manager
/**
 * \return seashell::AssetManager*
 */
AssetManager* Game::getAssetManager()
{
    return assetManager;
}

AudioManager* Game::getAudioManager()
{
    return audioManager;
}

EventManager* Game::getEventManager()
{
    return eventManager;
}

float Game::getSpeedFactor()
{
    return speedFactor;
}

//! Destructor
Game::~Game()
{
    device->drop();

    delete assetManager;
    delete audioManager;
    delete eventManager;
}

}
